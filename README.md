**Coadiey Bryan    C00039405**
# des

A Clojure app to encrypt and decrypt messages using DES.

## Usage

des only accepts Hexadecimal input for messages and keys at this time and please make sure they are each 64-bits long (ie. 16-Hex characters long).

```sh
 $ cd des
 $ java -jar ./target/des-0.0.1-SNAPSHOT-standalone.jar
```
### Installation

des requires [Java](https://www.java.com/en/) v1.6+ to run.
You can check your version by running `java -version`

License
----

GNU


**Free Software, Heck Yeah!**
