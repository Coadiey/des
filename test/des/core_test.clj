(ns des.core-test
  (:gen-class)
  (:require [clojure.test :refer :all]
            [des.core :refer :all]))

;; Coadiey Bryan C00039405

(deftest hex->binaryStr-test
  (testing "Binary with leading 0"
    (is (= "0110011111100001100111110100" (hex->binaryStr "67E19F4"))))
  (testing "Hexadecimal with leading 0s and binary with leading 0s"
    (is (= "0000000001100111111011110100" (hex->binaryStr "0067EF4")))))

(deftest binaryStr->hex-test
  (testing "Hex with leading 0s"
    (is (= "0000000001100111111011110100" (hex->binaryStr "0067EF4"))))
  (testing "Hexadecimal without leading 0s"
    (is (= "1110111101000000000001100111" (hex->binaryStr "EF40067")))))

(deftest permutation-test
  (testing "Identity test: Seq [1 2 3 4] on any hex should equal given hex"
    (is (= "5" (permutation [1 2 3 4] "5"))))
  (testing "Seq (1 1 1 1) on Hex 001 should equal Hex F"
    (is (= "f" (permutation '(12 12 12 12) "001"))))
  (testing "Seq [4 1 2 3] on Hex 5 should equal Hex A"
    (is (= "a" (permutation [4 1 2 3] "5"))))
  (testing "Seq [5 1 6 2 7 3 8 4] on Hex 5A should equal Hex 99"
    (is (= "99" (permutation [5 1 6 2 7 3 8 4] "5A")))))

;; Depends on permutation
(deftest leftCircularShift-test
  (testing "Hex without leading 0s in input, shift left 1 bit"                                                             
    (is (= "8780675" (leftCircularShift "c3c033a" 1))))
  (testing "Hex with leading 0s in output, shift left 1 bit"
    (is (= "0f00ceb" (leftCircularShift "8780675" 1))))  
  (testing "Hex without leading 0s in output, shift left 2 bits"
    (is (= "9f867d1" (leftCircularShift "cfc33e8" 1))))
  (testing "Hex with leading 0s in input, shift left 2 bits"
    (is (= "3c033ac" (leftCircularShift "0f00ceb" 2)))))

(deftest xor-test
  (testing "1111 XOR 0000 should return all 1s"
    (is (= "f" (xor "F" "0"))))
  (testing "0000 XOR 1111 should return all 1s"
    (is (= "f" (xor "0" "F"))))
  (testing "1111 XOR 1111 should return all 0s"
    (is (= "0" (xor "F" "F"))))
  (testing "0000 XOR 0000 should return all 1s"
    (is (= "0" (xor "0" "0")))))

(deftest getKeys-test
  (testing "Resulting list is length 16"
    (is (= 16 (count (getKeys "AABB09182736CCDD")))))
  (testing "Resulting keys in list should be length 12"                                                                             
    (is (= 12 (count (nth (getKeys "AABB09182736CCDD") 8)))))
  (testing "Given 0f1571c947d9e859 after second round should be 2b1a74ca48d8"            
    (is (= "2b1a74ca48d8" (nth (getKeys "0f1571c947d9e859") 1))))) 

(deftest sBox-test
  (testing "Resulting hex is length 8, 32-bits"
    (is (= 8 (count (sBox "711732E15CF0")))))
  (testing "Given hex 711732E15CF0 result should be C216D50"
    (is (= "0c216d50" (sBox "711732E15CF0"))))
  (testing "Given hex 965a847dcbd6 result should be 8afe657e"
    (is (= "8afe657e" (sBox "965a847dcbd6")))))

(deftest round-test
  (testing "Given message CC00CCFFF0AAF0AA and key 0B02679B49A5 result should be F0AAF0AA5E1CEC63"
    (is (= "F0AAF0AA5E1CEC63" (clojure.string/upper-case (round "CC00CCFFF0AAF0AA" "0B02679B49A5")))))
  (testing "Given message 14a7d67818ca18ad and key 194cd072de8c result should be 18CA18AD5A78E394"    
    (is (= "18CA18AD5A78E394" (clojure.string/upper-case (round "14a7d67818ca18ad" "194cd072de8c"))))))

(deftest encrypt-test 
  (testing "Given plaintext: 02468aceeca86420 and Key: 0f1571c947d9e859 Ciphertext is da02ce3a89ecac3b"
    (is (= "da02ce3a89ecac3b" (encrypt "02468aceeca86420" "0f1571c947d9e859"))))
  (testing "Given plaintext: 0123456789ABCDEF and Key: 0123456789ABCDEF Ciphertext is 56cc09e7cfdc4cef"
    (is (= "56cc09e7cfdc4cef" (encrypt "0123456789ABCDEF" "0123456789ABCDEF")))))

(deftest decrypt-test 
  (testing "Given ciphertext: da02ce3a89ecac3b and Key: 0f1571c947d9e859 Plaintext is 02468aceeca86420"
    (is (= "02468aceeca86420" (decrypt "da02ce3a89ecac3b" "0f1571c947d9e859"))))
  (testing "Given ciphertext: 56cc09e7cfdc4cef and Key: 0123456789ABCDEF Plaintext is 0123456789ABCDEF"
    (is (= "0123456789ABCDEF" (clojure.string/upper-case (decrypt "56cc09e7cfdc4cef" "0123456789ABCDEF"))))))


