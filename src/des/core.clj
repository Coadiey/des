(ns des.core
  (:gen-class)
  (:require [clojure.string :as str]))

;; Coadiey Bryan C00039405

;; Initial Permutation Table
(def ip '(58 50 42 34 26 18 10 2
           60 52 44 36 28 20 12 4
           62 54 46 38 30 22 14 6
           64 56 48 40 32 24 16 8
           57 49 41 33 25 17 9 1
           59 51 43 35 27 19 11 3
           61 53 45 37 29 21 13 5
           63 55 47 39 31 23 15 7))

(def ip1 '(40 8 48 16 56 24 64 32 
            39 7 47 15 55 23 63 31 
            38 6 46 14 54 22 62 30 
            37 5 45 13 53 21 61 29 
            36 4 44 12 52 20 60 28 
            35 3 43 11 51 19 59 27 
            34 2 42 10 50 18 58 26 
            33 1 41 9 49 17 57 25))

;; Permuted Choice 1 Table
(def pc1 '(57 49 41 33 25 17 9 
            1 58 50 42 34 26 18
            10 2 59 51 43 35 27
            19 11 3 60 52 44 36
            63 55 47 39 31 23 15
            7 62 54 46 38 30 22
            14 6 61 53 45 37 29
            21 13 5 28 20 12 4))

;; Permuted Choice 2 Table
(def pc2 '(14 17 11 24 1 5 3 28
            15 6 21 10 23 19 12 4
            26 8 16 7 27 20 13 2
            41 52 31 37 47 55 30 40
            51 45 33 48 44 49 39 56
            34 53 46 42 50 36 29 32))

;; Permutation Function Table
(def p '(16 7 20 21 29 12 28 17
          1 15 23 26 5 18 31 10
          2 8 24 14 32 27 3 9 
          19 13 30 6 22 11 4 25))

;; Expansion Permutation Table
(def ep '(32 1 2 3 4 5
           4 5 6 7 8 9
           8 9 10 11 12 13
           12 13 14 15 16 17
           16 17 18 19 20 21
           20 21 22 23 24 25
           24 25 26 27 28 29
           28 29 30 31 32 1))

;; Schedule of Left Shifts
(def shiftSchedule '(1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1))

;; S-boxes Table
(def  s-box [
             [[ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7 ], ;;s1
              [ 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8 ],
              [ 4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0 ],
              [ 15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 ]],
             [[ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10 ], ;;s2
              [ 3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5 ],
              [ 0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15 ],
              [ 13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 ]],
             [[ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8 ], ;;s3
              [ 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1 ],
              [ 13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7 ],
              [ 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]],
             [[ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15 ], ;;s4
              [ 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9 ],
              [ 10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4 ],
              [ 3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]],
             [[ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9 ], ;;s5
              [ 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6 ],
              [ 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14 ],
              [ 11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]],
             [[ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11 ], ;;s6
              [ 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8 ],
              [ 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6 ],
              [ 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]],
             [[ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1 ], ;;s7
              [ 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6 ],
              [ 1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2 ],
              [ 6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]],
             [[ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7 ], ;;s8
              [ 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2 ],
              [ 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8 ],
              [ 2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]]])     
       
;; String -> String
;; Convert hexadecimal to binary
(defn hex->binaryStr [hex]
  "Convert hexadecimal to binary"
  (let [expectedSize (* (count hex) 4)
        inputBits (Long/toBinaryString (Long/parseUnsignedLong hex 16))
        actualSize (count inputBits)]
    (if (= expectedSize actualSize)
      inputBits
      (loop [diff (- expectedSize actualSize) bin inputBits]
        (if (<= diff 0)
          bin
          (let [newBinary (str 0 bin) newDifference (dec diff)]
            (recur newDifference newBinary)))))))

;; String -> String
;; Convert binary to hexadecimal
(defn binaryStr->hex [binaryStr]
  "Convert hexadecimal to binary"
  (let [expectedSize (/ (count binaryStr) 4)
        inputHex (Long/toHexString (Long/parseUnsignedLong binaryStr 2))
        actualSize (count inputHex)
        difference (- expectedSize actualSize)] 
    (if (zero? difference) 
      inputHex
      (loop [diff difference hex inputHex]        
        (if (<= diff 0) 
          hex
          (let [newHex (str 0 hex) 
                newDifference (dec diff)]
            (recur newDifference newHex)))))))

;; Vector, String -> String
;; Given a sequence table as Vector and hexadecimal string
;; will translate hex into binary then rearrange 
;; bits of binary string according to sequence in
;; given table, result is then converted to hex and returned
(defn permutation [sequence, inputHex]
  "permutates hexadecimal input according to given sequence"
  (let [inputBits (hex->binaryStr inputHex)
        inputBitsLst (str/split inputBits #"")
        outputBitsLst (map #(nth inputBitsLst (dec %)) sequence)
        outputBits (str/join "" outputBitsLst)
        outputHex (binaryStr->hex outputBits)] 
    outputHex))

;; String, Int -> String
;; Given hexadecimal string and amount to of bits to shift left
;; shifts binary to the left circularly (first bit becomes last bit)
;; then returns hex string of result
(defn leftCircularShift [inputHex shiftNum]
  "Left circular shifts bits of given Hexadecimal string"
  (let [expectedShift (* (count inputHex) 4) ;; should be 28
        initialLst [1]
        permLst (loop [permutationLst initialLst actualShift (- expectedShift 2)]
                  (let [newPermLst (cons (+ actualShift 2) permutationLst)
                        newShift (dec actualShift)]
                    (if (< actualShift 0)
                      permutationLst                 
                      (recur newPermLst newShift))))
        output (loop [shift shiftNum inputStr inputHex]              
                 (if (> shift 0)                   
                   (recur (dec shift) (permutation permLst inputStr))
                   inputStr))]
    output))

;; String, String -> String
;; Given two hexadecimal strings converts them to binary
;; then Xors them together and returns hex representation 
;; of their result
(defn xor [hex1 hex2]
  "Xors the two given hex strings together"
  (let [binaryStr1 (hex->binaryStr hex1)
        binaryStr2 (hex->binaryStr hex2)
        initialSize (count binaryStr1)
        xorResult (loop [newBin "" i (dec (count binaryStr1))]
                    (if (< i 0) 
                      newBin
                      (let [char1 (nth binaryStr1 i)
                            char2 (nth binaryStr2 i)]
                        (if (= char1 char2)
                          (recur (str "0" newBin) (dec i))
                          (recur (str "1" newBin) (dec i))))))
        hexResult (binaryStr->hex xorResult)]
    hexResult))

;; String -> List
;; Given 64-bit Hex representation as String will initially convert to 56-bit key
;; then from this key a 48-bit key is generated for each round of DES after appropriate
;; key transformation is performed accordingly for that round. Returns a List containing
;; 16 keys as hexadecimal String representations
(defn getKeys [key]
  "Generates keys needed for every round of DES based on input hexadecimal key String"
  (let [permutedKey (permutation pc1 key)
        outKeyLst (loop [i 0 keys [] newKey permutedKey]
                    (if (< i 16)
                      (let [shiftedKey 
                            (str
                              (leftCircularShift (subs newKey 0 7) (nth shiftSchedule i))
                              (leftCircularShift (subs newKey 7 14) (nth shiftSchedule i)))
                            newKeyLst (conj keys (permutation pc2 shiftedKey))]
                        (recur (inc i) newKeyLst shiftedKey))
                      keys))]
    outKeyLst))

;; String -> String 
;; Given Hexadecimal String uses s-box lookup table to substitute values
;; according to what is in the lookup table Returns hex result as String
(defn sBox [hexStr]
  "S-box lookup on Hexadecimal String"
  (let [binaryStr (hex->binaryStr hexStr)
        output (loop [i 0 outStr ""]
                 (if (>= i 48)
                   outStr
                   (let [tmp (subs binaryStr i (+ i 6))
                         num (/ i 6)                         
                         row (Integer/parseInt (str (first tmp) "" (last tmp)) 2)
                         col (Integer/parseInt (subs tmp 1 5) 2)
                         outHex (Integer/toHexString (nth (nth (nth s-box num) row) col))
                         out (str outStr outHex)]
                     (recur (+ i 6) out))))]
    output))                     

;; String, String -> String
;; Given message hexadecimal as String and key hex as String
;; performs one round of DES and then returns resulting hexadecimal String
;; of the message
(defn round [inputStr key]
  "Performs one round of DES"
  (let [lhs (subs inputStr 0 8)
        rhs (subs inputStr 8 16)
        tmp rhs
        ;; Expansion Permutation
        tmpEp (permutation ep tmp)
        ;; expansion result XOR key for this round
        tmpXor (xor tmpEp key)
        ;; Substitution
        tmpSub (sBox tmpXor)
        ;; Permutation
        tmpP (permutation p tmpSub)
        ;; lhs XOR permuted result
        lhsXor (xor lhs tmpP)
        ;; rhs + lhsXor = result
        output (str rhs lhsXor)]
    output))
  
;; String, String -> String
;; Builds list of keys needed for every round of encryption, then performs initial 
;; permutation on plaintext, then does 16 rounds of encryption using that, then
;; swaps lhs and rhs of that result, and finally performs inverse permutation on
;; result of that swap, which results in the ciphertext output.
;; Interpretation: Given hexadecimal plaintext message and hex key both represented 
;; as Strings will perform DES encryption and return ciphertext String
(defn encrypt [plainText, key]
  "Encrypts plaintext message with DES algorithm using key"
  (println " After initial permutation:" (permutation ip, plainText))
  (let [keyLst (getKeys key)        
        plainTextAfterIp (permutation ip, plainText)        
        plainTextAfterRounds (loop [plainText plainTextAfterIp i 0]                               
                               (if (>= i 16)                                 
                                 plainText
                                 (let [keyForRound (nth keyLst i)
                                       roundResult (round plainText keyForRound)]
                                   (print "  Round" (inc i) " PlainText:" plainText " ")
                                   (println "Round key:" keyForRound)
                                   (recur roundResult (inc i)))))
        rhsOfResult (subs plainTextAfterRounds 8 16)
        lhsOfResult (subs plainTextAfterRounds 0 8)
        plainTextAfterSwap (str rhsOfResult lhsOfResult)
        cipherText (permutation ip1 plainTextAfterSwap)] 
    cipherText))
  
;; String, String -> String
;; Builds list of keys needed for every round of encryption, then performs initial 
;; permutation on plaintext, then does 16 rounds of encryption using that, then
;; swaps lhs and rhs of that result, and finally performs inverse permutation on
;; result of that swap, which results in the ciphertext output.
;; Interpretation: Given hexadecimal plaintext message and hex key both represented 
;; as Strings will perform DES encryption and return ciphertext String
(defn decrypt [cipherText, key]
  "Decrypts ciphertext message with DES algorithm using key"
  (println " After initial permutation:" (permutation ip, cipherText))
  (let [keyLst (getKeys key)        
        cipherTextAfterIp (permutation ip, cipherText)        
        cipherTextAfterRounds (loop [cipherText cipherTextAfterIp i 15]                               
                                (if (>= i 0)                    
                                  (let [keyForRound (nth keyLst i)
                                        roundResult (round cipherText keyForRound)]
                                    (print "  Round" (inc i) " PlainText:" cipherText " ")
                                    (println "Round key:" keyForRound)
                                    (recur roundResult (dec i)))
                                  cipherText))
        rhsOfResult (subs cipherTextAfterRounds 8 16)
        lhsOfResult (subs cipherTextAfterRounds 0 8)
        cipherTextAfterSwap (str rhsOfResult lhsOfResult)
        plainText (permutation ip1 cipherTextAfterSwap)]   
    plainText))

(defn -main []
  (do 
    (print "Please provide message to encrypt (Hexadecimal 16-length): ")
    (flush) 
    (def userMessage (read-line))
    (print "Please provide key to encrypt with (Hexadecimal 16-length): ")
    (flush)
    (def userKey (read-line)))
  (let [textHex userMessage
        keyHex userKey]
    (println "  Plain Text (Hex): " textHex " Key (Hex):" keyHex)
    (println "DES Encryption: ")
    (def cipherText (encrypt textHex keyHex)) 
    (println "\nCipher Text:" (clojure.string/upper-case cipherText)) 
    (println "\nDES Decryption: ")
    (def plainText (decrypt cipherText keyHex))
    (println "Plain Text:" (clojure.string/upper-case plainText))))

